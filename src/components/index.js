// import Banner from './Banner';
// import BottomTabBar from './BottomTabBar';
// import Carousel from './Carousel';
// import FullWidthImage from './FullWidthImage';
// import ImageCarousel from './ImageCarousel';
// import ImageSpin from './ImageSpin';
import * as Icon from "@expo/vector-icons";
import Loader from "./Loader";
import Notification from "./Notification";
// import PopupMessage from './PopupMessage';
// import ProductImage from './ProductImage';
// import RemoveButton from './RemoveButton';
// import ScrollContent from './ScrollContent';
// import ColourReel from './ColourReel';
import TouchableItem from "./TouchableItem";

export {
  // Banner,
  // BottomTabBar,
  // Carousel,
  // ColourReel,
  // FullWidthImage,
  Icon,
  // ImageCarousel,
  // ImageSpin,
  Loader,
  Notification,
  // PopupMessage,
  // ProductImage,
  // RemoveButton,
  // ScrollContent,
  TouchableItem,
};
