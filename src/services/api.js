import axios from "axios";
import Constants from "expo-constants";
import { expo } from "../../app.json";

const { manifest } = Constants;
const api =
  typeof manifest.packagerOpts === "object" && manifest.packagerOpts.dev
    ? `http://${manifest.debuggerHost
        .split(":")
        .shift()
        .concat(":8000")}/api/v1`
    : "api.example.com";
export default class {
  google = (data) => {
    let url;
    if (typeof data === typeof String()) {
      url = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${data}&key=${expo.android.config.googleMaps.apiKey}`;
    } else {
      url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${data.lat},${data.lng}&key=${expo.android.config.googleMaps.apiKey}`;
    }
    return axios
      .get(url)
      .then((response) => response.data)
      .catch((error) => error.response.data);
  };

  rapidApi = (lat, lng) => {
    const url = `https://geocodeapi.p.rapidapi.com/GetNearestCities?latitude=${lat}&longitude=${lng}&range=0`;
    return axios({
      url,
      method: "GET",
      headers: {
        "x-rapidapi-host": "geocodeapi.p.rapidapi.com",
        "x-rapidapi-key": "afaf62ae2bmsh16ac9cf2d2e3e4fp187a2fjsn3bd3e08130d1",
      },
    })
      .then((response) => response.data)
      .catch((error) => error);
  };

  logout() {
    this.http({
      url: "/logout",
      method: "post",
    }).catch(() => {});
    this.clear();
  }

  createAccount(data) {
    // {name: '', email: '', password: ''}
    return axios({
      url: `${api}/users/signup`,
      method: "post",
      data,
    })
      .then((response) => {
        const { user, token } = response.data;
        this.setToken(token);
        return {
          user,
          token,
        };
      })
      .catch((error) => {
        throw this.getError(error);
      });
  }

  socialLogin(user) {
    return axios({
      method: "post",
      url: `${api}/users/sociallogin`,
      user,
    })
      .then((response) => {
        console.log("Social Login response from backend", response.data);
        return response.data;
      })
      .catch((error) => {
        console.log("Social login error response from backend", error.response);
        return error.response;
      });
  }

  login(data) {
    this.token = null;
    this.loggedIn = false;
    return axios({
      method: "post",
      url: `${api}/users/login`,
      data,
    })
      .then((response) => {
        console.log("Response Data: ", response.data);
        return response.data;
      })

      .catch((error) => {
        console.log("ERROR: ", error.response.data);
      });
  }

  http({ endpoint, method, type, data, token }) {
    const config = {
      method,
      url: `${api}/${type}/${endpoint}`,
      data,
    };
    // console.log("config", config);
    if (token) {
      if (!config.headers) config.headers = {};
      config.headers.authorization = `Bearer ${token}`;
    }

    return axios(config)
      .then((response) => response.data)
      .catch((error) => {
        if (error.response) throw error.response.data;
        throw this.getError(error);
      });
  }
}
