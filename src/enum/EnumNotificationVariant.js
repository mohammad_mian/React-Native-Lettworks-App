import { Enum } from "enumify";

class EnumNotificationVariant extends Enum {}

EnumNotificationVariant.initEnum({
  NOTIFICATIONVARIANT_INFO: {
    /**
     * @return {string} string value representing enum value of info
     */

    get stringVal() {
      return "info";
    },
  },

  NOTIFICATIONVARIANT_ERROR: {
    /**
     * @return {string} string value representing enum value of error
     */

    get stringVal() {
      return "error";
    },
  },

  NOTIFICATIONVARIANT_WARNING: {
    /**
     * @return {string} string value representing enum value of warning
     */

    get stringVal() {
      return "warning";
    },
  },

  NOTIFICATIONVARIANT_SUCCESS: {
    /**
     * @return {string} string value representing enum value of success
     */

    get stringVal() {
      return "success";
    },
  },

  NOTIFICATIONVARIANT_TYPES: {
    /**
     * @return {object} object representing all enum values
     */
    get data() {
      return [
        EnumNotificationVariant.NOTIFICATIONVARIANT_INFO,
        EnumNotificationVariant.NOTIFICATIONVARIANT_ERROR,
        EnumNotificationVariant.NOTIFICATIONVARIANT_WARNING,
        EnumNotificationVariant.NOTIFICATIONVARIANT_SUCCESS,
      ];
    },
  },
});

export default EnumNotificationVariant;
