import { DateTime } from "luxon";
import uuid from "uuid-random";
import EnumNotificationVariant from "enum/EnumNotificationVariant";

class ModelNotification {
  /**
   * @param {ModelNotification} model Model to output
   * @return {object} object output of model
   */

  static outputModel(model) {
    return {
      enumNotificationVariant: model.getVariant(),
      strKey: model.getKey(),
      strMessage: model.getMessage(),
    };
  }

  /**
   * Instantiates ModelNotification
   *
   * @param {string} strMessage Notification message
   * @param {EnumNotificationVariant} enumNotificationVariant Notification variant
   * @param {string} strKey Unique notificaation key
   * @return {undefined}
   */

  constructor(
    strMessage,
    enumNotificationVariant = EnumNotificationVariant.NOTIFICATION_INFO,
    strKey = `${DateTime.utc().toISODate()}${uuid()}`
  ) {
    this.strMessage = strMessage;
    this.enumNotificationVariant = enumNotificationVariant;
    this.strKey = strKey;
  }

  /**
   * @return {string} key
   */
  getKey() {
    return this.strKey;
  }

  /**
   * @return {string} message
   */

  getMessage() {
    return this.strMessage;
  }

  /**
   * @return {EnumNotificationVariant} variant
   */

  getVariant() {
    return this.enumNotificationVariant;
  }
}

export default ModelNotification;
