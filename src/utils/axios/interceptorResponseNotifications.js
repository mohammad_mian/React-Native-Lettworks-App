import ModelNotification from "model/ModelNotification";
import EnumNotificationVariant from "enum/EnumNotificationVariant";
import { actionEnqueueNotification } from "redux/actions/actionNotification";

/**
 * @param {axios} axios Axios instance
 * @param {func} dispatch Redux action dispatcher
 * @param {?string} ustrDefaultSuccessMessage Default success message to display if none is set by Api
 * @param {?string} ustrDefaultErrorMessage Default error message to display if none is set by Api
 * @param {boolean} bIsCancel has the request been cancelled
 * @return {undefined}
 */

export const createInterceptorResponseNotifications = (
  axios,
  dispatch,
  ustrDefaultSuccessMessage = undefined,
  ustrDefaultErrorMessage = undefined
) => {
  axios.interceptors.response.use(
    (successResponse) => {
      console.log("success response!", successResponse);
      let ustrSuccessMessage = ustrDefaultSuccessMessage;

      if (
        successResponse &&
        successResponse.data &&
        successResponse.data.status === "success" &&
        "string" === typeof successResponse.data.status
      ) {
        ustrSuccessMessage = successResponse.data.message;
      }

      if (undefined === ustrSuccessMessage || null === ustrSuccessMessage) {
        return successResponse;
      }

      const modelNotificationSuccess = new ModelNotification(
        ustrSuccessMessage,
        EnumNotificationVariant.NOTIFICATIONVARIANT_SUCCESS
      );

      dispatch(actionEnqueueNotification(modelNotificationSuccess));
    },
    (error) => {
      let ustrErrorMessage = ustrDefaultErrorMessage;
      if (
        error &&
        error.response &&
        error.response.data &&
        error.response.data.message &&
        "string" === typeof error.response.data.message
      ) {
        ustrErrorMessage = error.response.data.message;
      }

      if (
        error &&
        error.response &&
        error.response.data &&
        error.response.data.errors &&
        "object" === typeof error.response.data.errors
      ) {
        const arrAllFieldErrors = Object.values(error.response.data.errors);
        const arrFlattenedErrors = arrAllFieldErrors.flat();
        ustrErrorMessage = arrFlattenedErrors.join(" ");
      }

      if (
        error &&
        error.response &&
        error.response.data &&
        error.response.data.errors &&
        Array.isArray(error.response.data.errors) &&
        error.response.data.errors.length
      ) {
        ustrErrorMessage = error.response.data.errors[0].detail;
      }

      if (undefined === ustrErrorMessage || null === ustrErrorMessage) {
        return Promise.reject(error);
      }

      const modelNotificationError = new ModelNotification(
        ustrErrorMessage,
        EnumNotificationVariant.NOTIFICATIONVARIANT_ERROR
      );

      dispatch(actionEnqueueNotification(modelNotificationError));

      return Promise.reject(error);
    }
  );
};
