import axios from "axios";
import { expo } from "../../../app.json";
import { apiUrl } from "config/apiConfig";
const REQUEST_TIMEOUT_DURATION_MS = 10 * 1000;
/**
 * Handle instantiating axios, and applying defaults to be used across the application.
 * @param {func} dispatch Action dispatcher
 * @param {func} getState Redux state accessor
 * @param {?string} ustrDefaultSuccessMessage default success message to display if none set by API
 * @param {?string} ustrDefaultErrorMessage default error message to display if none set by API
 * @param {string} strContentType content type header
 * @return {axios} Instance of axios
 */
export const instanceAxios = (getState, strContentType) => {
  // Instantiate axios using some default headers
  const axiosInstance = axios.create({
    baseURL: `${apiUrl}/`,
    headers: {
      Accept: "application/json",
      "Content-Type": strContentType || "application/json",
      "app-access-token": expo.extra.API_TOKEN,
    },
    timeout: REQUEST_TIMEOUT_DURATION_MS,
  });

  // Setup interceptors (to automatically handle inserting up-to-date access tokens)
  axiosInstance.interceptors.request.use(
    (config) => {
      const objConfig = config;
      objConfig.headers.Authorization = `Bearer ${getState().auth.token}` || "";

      return objConfig;
    },
    (error) => {
      console.log("I am coming from createInterceptotoken");
      Promise.reject(error);
    }
  );

  // //The instance if axios we have created does not have the isCancel Helper function
  // //so we'll need to pass the value returned in seprately
  // // const bIsCancel = axios.isCancel(axiosInstance);

  // createInterceptorResponseNotifications(
  //   axiosInstance,
  //   dispatch,
  //   ustrDefaultSuccessMessage,
  //   ustrDefaultErrorMessage
  // );

  return axiosInstance;
};

// export const instanceAxios = (getState, strContentType) => {
//   const axiosInstance = axios.create({
//     baseUrl: `${apiUrl}/`,
//     headers: {
//       Accept: "application/json",
//       "Content-Type": strContentType || "application/json",
//       "app-access-token": expo.extra.API_TOKEN,
//     },
//     timeout: REQUEST_TIMEOUT_DURATION_MS,
//   });

//   const requestHandler = (request) => {
//     request.headers.Authorization = `Bearer ${getState().auth.token}` || "";
//     return request;
//   };

//   return axiosInstance.interceptors.request.use((request) =>
//     requestHandler(request)
//   );
// };
