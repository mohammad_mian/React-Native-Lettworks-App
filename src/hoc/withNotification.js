import React from "react";
import PropTypes from "prop-types";
import {
  actionEnqueueNotification,
  actionRemoveNotification,
} from "../redux/actions/actionNotification";
import { connect } from "react-redux";
/**
 * @param {WrappedComponent} WrappedComponent Component to be passed props
 * @return {React.Component} React component
 */

const withNotification = (WrappedComponent) => {
  class HOC extends React.Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  //Format name
  HOC.displayName = `withNotification(
    ${WrappedComponent.displayName || WrappedComponent.name}
  )`;

  HOC.defaultProps = {};
  HOC.propTypes = {
    enqueueNotification: PropTypes.func.isRequired,
    removeNotification: PropTypes.func.isRequired,
    objNotification: PropTypes.object,
  };

  /**
   * @param {disptach} dispatch dispatch actions
   * @return {object} props to be passsed to wrappedComponent
   */

  const mapDispatchToProps = (dispatch) => ({
    enqueueNotification: (notification) =>
      dispatch(actionEnqueueNotification(notification)),
    removeNotification: () => dispatch(actionRemoveNotification()),
  });

  /**
   * @param {disptach} dispatch dispatch actions
   * @return {object} props to be passsed to wrappedComponent
   */

  const mapStateToProps = (state, ownProps) => ({
    objNotification: state.notification.objNotification,
  });

  return connect(mapStateToProps, mapDispatchToProps)(HOC);
};

export default withNotification;
