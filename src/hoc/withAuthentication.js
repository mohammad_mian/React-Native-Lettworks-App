/**
 * @author HMian
 */

import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import {
  actionLogin,
  actionGoogleLogin,
  actionFacebookLogin,
  actionForgotPassword,
  actionCreateAccount,
  actionPreLogout,
  actionLogout,
  actionClearAuthentication,
  actionNavigateResetPasswordAuth,
  actionResetPasswordAuth,
} from "redux/actions/actionAuth";

/**
 * Hoc for wrapping a component in a Redux-connector
 * @param {React.component} WrappedComponent React component to wrap in
 * @return {func} Function wrapping react component
 */

const withAuthentication = (WrappedComponent) => {
  class HOC extends React.Component {
    /**
     * Renders the component
     * @return {JSX} Component's markup
     */

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  //Format name
  HOC.displayName = `withAuthentication(${
    WrappedComponent.displayName || WrappedComponent.name
  })`;

  HOC.defaultProps = {};

  HOC.propTypes = {
    userLoginRequest: PropTypes.func.isRequired,
    userGoogleLoginRequest: PropTypes.func.isRequired,
    userFacebookLoginRequest: PropTypes.func.isRequired,
    userCreateAccountRequest: PropTypes.func.isRequired,
    userPreLogoutRequest: PropTypes.func.isRequired,
    userLogoutRequest: PropTypes.func.isRequired,
    userClearAuthRequest: PropTypes.func.isRequired,
    authenticated: PropTypes.bool,
    token: PropTypes.string,
    loginType: PropTypes.string,
    unauthorised: PropTypes.bool,
    authenticating: PropTypes.bool,
    loggingOut: PropTypes.bool,
    error: PropTypes.bool,
    resetPassword: PropTypes.bool,
  };

  /**
   *
   * @param {dispatch} dispatch Action dispatcher
   * @return {object} Object mapping actions to component props
   */

  const mapDispatchToProps = (dispatch) => ({
    userLoginRequest: (userDetails) => dispatch(actionLogin(userDetails)),
    userGoogleLoginRequest: () => dispatch(actionGoogleLogin()),
    userFacebookLoginRequest: () => dispatch(actionFacebookLogin()),
    userCreateAccountRequest: (userData) =>
      dispatch(actionCreateAccount(userData)),
    userPreLogoutRequest: () => dispatch(actionPreLogout()),
    userLogoutRequest: () => dispatch(actionLogout()),
    userClearAuthRequest: () => dispatch(actionClearAuthentication()),
    userForgotPasswordRequest: (data) => dispatch(actionForgotPassword(data)),
    navigateResetPassword: () => dispatch(actionNavigateResetPasswordAuth()),
    userResetPasswordRequest: (data) => dispatch(actionResetPasswordAuth(data)),
  });

  /**
   * @param {object} state Redux state
   * @param {object} ownProps Props passed directly to the component
   * @return {object} Combined object of props provided to the component
   */

  const mapStateToProps = (state, ownProps) => ({
    authenticated: state.auth.authenticated,
    token: state.auth.token,
    loginType: state.auth.loginType,
    unauthorised: state.auth.unauthorised,
    authenticating: state.auth.authenticating,
    loggingOut: state.auth.loggingOut,
    error: state.auth.error,
    resetPassword: state.auth.resetPassword,
  });

  return connect(mapStateToProps, mapDispatchToProps)(HOC);
};

export default withAuthentication;
