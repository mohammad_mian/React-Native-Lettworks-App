import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { actionUpdateUser } from "redux/actions/actionUser";

const withUser = (WrappedComponent) => {
  class HOC extends React.Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  //Format hoc name
  HOC.displayName = `withUser${
    WrappedComponent.displayName || WrappedComponent.name
  }`;
  // Validate prop types
  HOC.propTypes = {
    updateUser: PropTypes.func.isRequired,
    id: PropTypes.string,
    role: PropTypes.string,
    isActive: PropTypes.bool,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    preferences: PropTypes.object,
    pushToken: PropTypes.object,
    appVersion: PropTypes.string,
  };

  /**
   *
   * @param {dispatch} dispatch Action dispatcher
   * @return {object} Object mapping actions to component props
   */

  const mapDispatchToProps = (dispatch) => ({
    updateUser: () => dispatch(actionUpdateUser()),
  });

  /**
   * @param {object} state Redux state
   * @param {object} ownProps Props passed directly to the component
   * @return {object} Combined object of props provided to the component
   */

  const mapStateToProps = (state, ownProps) => ({
    id: state.user._id,
    role: state.user.role,
    isActive: state.user.isActive,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    email: state.user.email,
    preferences: state.user.preferences,
    pushToken: state.user.pushToken,
    appVersion: state.user.appVersion,
  });

  return connect(mapStateToProps, mapDispatchToProps)(HOC);
};

export default withUser;
