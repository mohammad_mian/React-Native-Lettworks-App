import api from "services/api";
import * as Google from "expo-google-app-auth";
import * as Facebook from "expo-facebook";
import { expo } from "../../../app.json";
import * as Types from "./types";
import { instanceAxios } from "utils/axios/instanceAxios";
import ModelNotification from "model/ModelNotification";
import EnumNotificationVariant from "enum/EnumNotificationVariant";
import axios from "axios";

const facebookGraphApiUrl = (token) =>
  `https://graph.facebook.com/me?access_token=${token}&fields=id,name,email`;

const dispatchCancelLogin = (dispatch) => {
  dispatch({ type: Types.REMOVE_NOTIFICATION });
  return dispatch({ type: Types.CLEAR_AUTH });
};

const onLginError = (dispatch, strMessage, variant) => {
  const errorNotification = new ModelNotification(strMessage, variant);
  dispatch({
    type: Types.ENQUEUE_NOTIFICATION,
    payload: errorNotification,
  });
  dispatch({ type: Types.LOGIN_ERROR });
};

export const actionGoogleLogin = () => {
  return async (dispatch, getState) => {
    dispatch({
      type: Types.AUTHENTICATING,
    });

    try {
      const result = await Google.logInAsync({
        iosClientId: expo.extra.IOS_CLIENT_ID,
        androidClientId: expo.extra.ANDROID_CLIENT_ID,
        scopes: ["profile", "email"],
      });
      const { type, user } = result;
      if (type === "success") {
        // console.log("Google result", result);
        // Google login was successful
        const iAxios = instanceAxios(getState);

        const { data } = await iAxios.post("users/sociallogin", user);
        if (data.status === "success") {
          dispatch({
            type: Types.LOGIN,
            payload: {
              loginType: "social",
              ...data,
            },
          });
        }
      } else {
      }
    } catch ({ message }) {
      if (message.includes("ERR_APP_AUTH"))
        return dispatchCancelLogin(dispatch);
      // Goolge server is down
      onLginError(
        dispatch,
        "Sorry, Google server is currently down. Please sign in using another method.",
        EnumNotificationVariant.NOTIFICATIONVARIANT_ERROR
      );
    }
  };
};

export function actionFacebookLogin() {
  return async (dispatch, getState) => {
    dispatch({
      type: Types.AUTHENTICATING,
    });
    try {
      await Facebook.initializeAsync(expo.extra.facebookAppId);
      const {
        type,
        token: facbeookToken,
        expires,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ["public_profile", "email"],
      });

      if (type === "success") {
        const url = facebookGraphApiUrl(facbeookToken);
        const userDetails = await axios.get(url);
        const iAxios = instanceAxios(getState);
        const { data } = await iAxios.post(
          "users/socialLogin",
          userDetails.data
        );
        const { token, user, status } = data;
        // const response = await api.login({ email, password });
        dispatch({
          type: Types.LOGIN,
          payload: {
            loginType: "social",
            user,
            status,
            token,
          },
        });
      } else {
        dispatchCancelLogin(dispatch);
      }
    } catch (error) {
      onLginError(
        dispatch,
        "Sorry, Facebook server is currently down. Please sign in using another method.",
        EnumNotificationVariant.NOTIFICATIONVARIANT_ERROR
      );
    }
  };
}

export function actionLogin(userDetails) {
  return async (dispatch, getState) => {
    dispatch({
      type: Types.AUTHENTICATING,
    });
    try {
      const iAxios = instanceAxios(getState);
      const { data } = await iAxios.post(`users/login`, userDetails);
      const { token, user, status } = data;
      dispatch({
        type: Types.LOGIN,
        payload: {
          loginType: "login",
          token,
          user,
        },
      });
    } catch (error) {
      let strMessage =
        "Sorry, server is currently down. Please sign in using another method.";
      if (error.response.data && error.response.data.error.status === "fail") {
        strMessage = error.response.data.error.message;
      }
      return onLginError(
        dispatch,
        strMessage,
        EnumNotificationVariant.NOTIFICATIONVARIANT_ERROR
      );
    }
  };
}

export function actionCreateAccount(userData) {
  return async (dispatch, getState) => {
    try {
      const iAxios = instanceAxios(getState);
      const { data } = await iAxios.post("users/signup", userData);
      const { token, user } = data;
      dispatch({
        type: Types.LOGIN,
        payload: {
          loginType: "register",
          token,
          user,
        },
      });
    } catch (error) {
      let strMessage =
        "Sorry, server is currently down. Please sign in using another method.";
      if (error.response.data && error.response.data.error.status === "fail") {
        strMessage = error.response.data.error.message;
      }
      return onLginError(
        dispatch,
        strMessage,
        EnumNotificationVariant.NOTIFICATIONVARIANT_ERROR
      );
    }
  };
}

export const actionForgotPassword = (userDetails) => {
  return async (dispatch, getState) => {
    dispatch({
      type: Types.AUTHENTICATING,
    });
    try {
      const iAxios = instanceAxios(getState);
      const { data } = await iAxios.post("users/forgotpassword", userDetails);
      const successNotification = new ModelNotification(
        data.message,
        EnumNotificationVariant.NOTIFICATIONVARIANT_SUCCESS
      );
      dispatch({
        type: Types.ENQUEUE_NOTIFICATION,
        payload: successNotification,
      });
    } catch (error) {
      let strMessage =
        "Sorry, server is currently down. Please sign in using another method.";
      if (error.response.data && error.response.data.error.status === "fail") {
        strMessage = error.response.data.error.message;
      }
      return onLginError(
        dispatch,
        strMessage,
        EnumNotificationVariant.NOTIFICATIONVARIANT_ERROR
      );
    }
  };
};

export function actionPreLogout() {
  return {
    type: Types.PRE_LOGOUT,
  };
}

export function actionNavigateResetPasswordAuth() {
  return {
    type: Types.RESETPASSWORD_AUTH,
  };
}
export function actionResetPasswordAuth(userdDetails) {
  return async (dispatch, getState) => {
    dispatch({
      type: Types.AUTHENTICATING,
    });
    try {
      const iAxios = instanceAxios(getState);
      const { data } = await iAxios.post(
        `users/resetpassword/${userDetails.token}`,
        userdDetails
      );
      const { user, token, status } = data;
      dispatch({
        type: Types.LOGIN,
        payload: {
          user,
          token,
          status,
        },
      });
    } catch (error) {
      let strMessage =
        "Sorry, server is currently down. Please sign in using another method.";
      if (error.response.data && error.response.data.error.status === "fail") {
        strMessage = error.response.data.error.message;
      }
      return onLginError(
        dispatch,
        strMessage,
        EnumNotificationVariant.NOTIFICATIONVARIANT_ERROR
      );
    }
  };
}
export function actionLogout() {
  return async (dispatch) => {
    dispatch(preLogout());
    setTimeout(() => {
      api.logout();
      dispatch({
        type: Types.LOGOUT,
      });
    });
  };
}

export function actionClearAuthentication() {
  return {
    type: Types.CLEAR_AUTH,
  };
}
