import { ENQUEUE_NOTIFICATION, REMOVE_NOTIFICATION } from "./types";

/**
 * @param {ModelNotification} modelNotification Notification to enqueue
 * @return {object} Action data
 */

export const actionEnqueueNotification = (modelNotification) => {
  return async (dispatch, getState) => {
    dispatch({
      payload: {
        ...modelNotification,
      },
      type: ENQUEUE_NOTIFICATION,
    });
  };
};

export const actionRemoveNotification = () => {
  return async (dispatch, getState) => {
    dispatch({
      type: REMOVE_NOTIFICATION,
    });
  };
};
