import * as Types from "redux/actions/types";

/**
 * @param {null} state Redux state
 * @param {object} action Redux action
 * @return {object} Reduced state
 */

export default function (state = { authenticated: false }, action) {
  switch (action.type) {
    case Types.LOGIN:
      return {
        authenticated: true,
        token: action.payload.token,
        loginType: action.payload.loginType,
        authenticating: false,
      };
    case Types.AUTHENTICATING:
      return { ...state, gauthenticatin: true };
    case Types.LOGIN_ERROR:
      return { error: true };
    case Types.PRE_LOGOUT:
      return { ...state, loggingOut: true };
    case Types.CLEAR_AUTH:
      return { authenticated: false };
    case Types.LOGOUT:
      return null;
    case Types.RESETPASSWORD_AUTH:
      return { ...state, resetPassword: true };
    default:
      return state;
  }
}
