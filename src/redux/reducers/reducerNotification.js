import { ENQUEUE_NOTIFICATION, REMOVE_NOTIFICATION } from "../actions/types";

/**
 * @param {object} state Redux state
 * @param {object} action Redux action
 * @return {object} Reduced state
 */

export default function (state = {}, action) {
  switch (action.type) {
    case ENQUEUE_NOTIFICATION:
      return {
        objNotification: action.payload,
      };
    case REMOVE_NOTIFICATION:
      return {};
    default:
      return state;
  }
}
