import { persistCombineReducers } from "redux-persist";
import { AsyncStorage } from "react-native";
import { isDev } from "config/functions";
import auth from "./reducerAuth";
import user from "./reducerUser";
import notification from "./reducerNotification";

const rootReducer = persistCombineReducers(
  {
    key: isDev() ? "lettworks-app" : "lettworks",
    storage: AsyncStorage,
    timeout: 0,
  },
  {
    auth,
    user,
    notification,
  }
);

export default rootReducer;
