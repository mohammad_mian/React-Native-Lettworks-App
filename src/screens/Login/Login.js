import React from "react";
import { StyleSheet, View, Keyboard } from "react-native";
import { Linking } from "expo";
import { Notification } from "components";
import { Button, Input } from "atoms";
import { validate } from "config/validation";
import LoginWrapper from "./LoginWrapper";
import withAuthentication from "hoc/withAuthentication";
import withNotification from "hoc/withNotification";
import { compose } from "redux";

class LoginScreen extends React.PureComponent {
  state = {
    email: "",
    password: "",
    errors: [],
    submitted: false,
  };

  componentDidMount() {
    this.calcErrors(); // Validates email and password in the state and sets errors array to ['email', 'password'] if error exists
    this.props.userClearAuthRequest(); // Action to reduce auth state to an object
    Linking.getInitialURL().then(this.onReset); // Deep linking if  app is NOT already open navigated to reset password// seperate path and query params if path = 'reset-password' navigate to reset screen and pass query params
    Linking.addEventListener("url", (obj) => {
      this.onReset(obj.url); // If app is open
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.authenticated && !prevProps.authenticated) {
      return navigation.navigate("Main");
    }
  }

  onReset = (url) => {
    const { path, queryParams } = Linking.parse(url);
    if (path === "reset-password") {
      this.props.navigation.navigate("Reset", queryParams);
    }
  };

  onLogin = () => {
    const errors = this.getErrors();
    if (errors.length) return this.setState({ submitted: true, errors });

    if (this.props.authenticating) return;
    const { email, password } = this.state;
    Keyboard.dismiss();
    this.props.userLoginRequest({ email, password });
  };

  onBack = () => this.props.navigation.navigate("Start");

  onRegister = () => {
    this.props.navigation.navigate("Register");
  };

  onForgot = () => {
    this.props.userClearAuthRequest();
    this.props.navigation.navigate("Forgot");
  };

  onChange = (field, value) => {
    this.setState({ [field]: value }, this.calcErrors);
  };

  calcErrors = () => {
    this.setState({ errors: this.getErrors() });
  };

  getErrors = () => {
    const errors = [];
    if (!validate("email", this.state.email)) errors.push("email");
    if (!validate("password", this.state.password)) errors.push("password");
    return errors;
  };

  getActionButtons = () => (
    <>
      <Button
        medium
        style={styles.actionButton}
        onPress={this.onRegister}
        colour="noBorderBackground"
      >
        Haven't got an account? Register
      </Button>
    </>
  );

  clearAuthAndNotification = () => {
    this.props.userClearAuthRequest();
    this.props.removeNotification();
  };

  render() {
    const {
      userClearAuthRequest,
      error,
      authenticating,
      objNotification,
    } = this.props;
    const { submitted, errors, email, password } = this.state;

    return (
      <View style={styles.container}>
        <LoginWrapper actions={this.getActionButtons()}>
          <View>
            <Input
              v1
              style={styles.inputBox}
              inputValid={!errors.includes("email")}
              error={submitted && errors.includes("email")}
              labelStyle={styles.labelStyle}
              label="Email Address"
              value={email}
              autoCorrect={false}
              ref={(r) => (this.email = r)}
              keyboardType="email-address"
              autoCapitalize="none"
              onChangeText={(val) => this.onChange("email", val)}
            />

            <Input
              v1
              style={styles.inputBox}
              inputValid={!errors.includes("password")}
              error={submitted && errors.includes("password")}
              labelStyle={styles.labelStyle}
              label="Password"
              value={password}
              ref={(r) => (this.password = r)}
              secureEntry
              iconColour="white"
              onChangeText={(val) => this.onChange("password", val)}
            />

            <Button
              medium
              loading={authenticating}
              onPress={this.onLogin}
              colour="primary"
            >
              Sign In
            </Button>
            <View style={styles.buttons}>
              <Button
                smallTextSize
                style={{ marginTop: 20, marginRight: 10 }}
                onPress={this.onBack}
                colour="transparent"
              >
                Go Back
              </Button>
              <Button
                smallTextSize
                style={{ marginTop: 20 }}
                onPress={this.onForgot}
                colour="transparent"
              >
                Forgot Password?
              </Button>
            </View>
          </View>
        </LoginWrapper>
        {error && objNotification && (
          <Notification
            onClose={this.clearAuthAndNotification}
            active={error && objNotification}
            type={objNotification.enumNotificationVariant.stringVal}
            autoClose
            message={objNotification.strMessage}
          />
        )}
      </View>
    );
  }
}

export default compose(withAuthentication, withNotification)(LoginScreen);

const styles = StyleSheet.create({
  buttons: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  popupRow: {
    marginVertical: 10,
  },
  popupText: {
    fontSize: 20,
    fontFamily: "Bold",
    color: "#000",
  },
  skipButton: {
    paddingVertical: 20,
    paddingRight: 20,
  },
  skip: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    width: "100%",
  },
  confirm: {
    flexDirection: "row",
  },
  confirmButton: {
    marginHorizontal: 10,
  },
  inputBox: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
  },
  labelStyle: {
    color: "#FFFFFF",
    fontSize: 10,
  },
  actionButton: {
    marginHorizontal: 10,
    width: "40%",
    borderWidth: 0,
  },
});
