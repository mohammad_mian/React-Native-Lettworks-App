import React from "react";
import { StyleSheet, View, Keyboard } from "react-native";
import { Notification } from "components";
import { compose } from "redux";
import withAuthentication from "hoc/withAuthentication";
import withNotification from "hoc/withNotification";
import { Button, Input, Text, BlankButton } from "atoms";
import { validate } from "config/validation";
import { Linking } from "expo";
import LoginWrapper from "./LoginWrapper";
class ForgotPasswordScreen extends React.PureComponent {
  state = {
    email: "",
    error: false,
    errors: [],
  };

  componentDidMount() {
    this.calcErrors();
  }

  componentDidUpdate() {
    if (this.props.resetPassword)
      return this.props.navigation.navigate("Login");
  }

  onReset = async () => {
    const errors = this.getErrors();
    if (errors.length) return this.setState({ submitted: true, errors });
    if (this.state.loading) return;
    this.setState({ loading: true });
    Keyboard.dismiss();
    // Create a deep link url
    const deepLinkUrl = Linking.makeUrl("/reset"); // exp://192.168.0.23:19000/--/reset
    await this.props.userForgotPasswordRequest({
      email: this.state.email,
      deepLinkUrl,
    });
    // console.log("Forgot password response", response);
    this.setState(
      { success: true, loading: false, email: "" },
      this.calcErrors
    );
  };

  calcErrors = () => {
    this.setState({ errors: this.getErrors() });
  };

  getErrors = () => {
    const errors = [];
    if (!validate("email", this.state.email)) errors.push("email");
    return errors;
  };

  onChange = (field, value) => {
    this.setState({ [field]: value }, this.calcErrors);
  };

  onLogin = () => {
    this.props.navigation.goBack();
  };

  onCloseError = () => {
    this.setState({ error: false });
  };

  onCloseSuccess = () => {
    this.setState({ success: false });
  };

  getActionButtons = () => (
    <View style={styles.center}>
      <BlankButton onPress={this.onLogin}>
        <Text style={styles.blankButton}>Remembered it? Log in here</Text>
      </BlankButton>
    </View>
  );

  clearAuthAndNotification = () => {
    this.props.userClearAuthRequest();
    this.props.removeNotification();
    this.props.navigateResetPassword();
  };

  render() {
    const { errors, submitted, email, error, loading, success } = this.state;
    const { objNotification, authenticating } = this.props;

    return (
      <View style={styles.container}>
        <LoginWrapper actions={this.getActionButtons()}>
          <View style={styles.form}>
            <View style={styles.center}>
              <Text style={styles.title}>Reset Password</Text>
            </View>
            <Input
              v1
              inputValid={!errors.includes("email")}
              error={submitted && errors.includes("email")}
              style={styles.inputBox}
              labelStyle={styles.labelStyle}
              label="Email Address"
              value={email}
              keyboardType="email-address"
              autoCapitalize="none"
              onChangeText={(val) => this.onChange("email", val)}
            />

            <Button
              medium
              loading={loading}
              onPress={this.onReset}
              colour="primary"
              disabled={authenticating}
            >
              Reset Password
            </Button>
          </View>
        </LoginWrapper>
        {objNotification && (
          <Notification
            onClose={this.clearAuthAndNotification}
            active={objNotification}
            type={objNotification.enumNotificationVariant.stringVal}
            autoClose
            message={objNotification.strMessage}
          />
        )}
      </View>
    );
  }
}

export default compose(
  withAuthentication,
  withNotification
)(ForgotPasswordScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  form: {
    marginBottom: 40,
  },
  center: {
    width: "100%",
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    fontFamily: "Bold",
    color: "#fff",
    marginBottom: 20,
  },
  inputBox: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
  },
  labelStyle: {
    color: "#FFFFFF",
    fontSize: 10,
  },
  blankButton: {
    fontFamily: "Medium",
    color: "#fff",
  },
});
