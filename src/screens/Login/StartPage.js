import React from "react";
import { compose } from "redux";
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  SafeAreaView,
} from "react-native";
import { Button } from "atoms/index";
import Layout from "config/constants";
import { Video } from "expo-av";
import { Videos } from "config/AppVideos";
import { Notification, Loader } from "components";
import { ImagesIntro } from "config/AppIntro";
import { ImagesLogos } from "config/AppIcons/AppLogos";
import { socialIcons } from "config/AppIcons/AppIcons";
import withAuthentication from "hoc/withAuthentication";
import withNotification from "hoc/withNotification";
class StartPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      play: true,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.blurListener = navigation.addListener("didBlur", () => {
      this.setState({ play: false });
    });
    this.focusListener = navigation.addListener("willFocus", () => {
      this.setState({ play: true });
    });

    console.log("I am coming from StartPage.js");
  }

  componentDidUpdate(prevProps) {
    const { authenticated, navigation, loginType } = this.props;
    if (!authenticated) return;
    if (
      (authenticated && !prevProps.authenticated && loginType === "register") ||
      loginType === "social"
    ) {
      return navigation.navigate("Onboarding");
    } else {
      return navigation.navigate("Main");
    }
  }

  componentWillUnmount() {
    this.focusListener.remove();
    this.blurListener.remove();
  }

  onLogin = () => this.props.navigation.navigate("Login");

  onRegister = () => this.props.navigation.navigate("Register");

  facebookLoginHandler = () => this.props.userFacebookLoginRequest();

  googleLoginHandler = () => this.props.userGoogleLoginRequest();

  renderVideo() {
    const { play } = this.state;

    return (
      <Video
        rate={1.0}
        volume={0}
        isMuted
        resizeMode="cover"
        shouldPlay={play}
        isLooping
        // onError={()=>this.onVideoError()}
        style={styles.fullScreen}
        source={Videos.startSmaller2}
      />
    );
  }

  // renderErrorNotifications = () => {
  //   const { arrNotifications, userClearAuthRequest } = this.props;
  //   console.log("arrNotifications", arrNotifications);
  //   return arrNotifications.map((notification, i) => (
  //     <Notification
  //       key={i}
  //       onClose={userClearAuthRequest}
  //       active={arrNotifications && arrNotifications.length > 0}
  //       type={notification.enumNotificationVariant.stringVal}
  //       message={notification.strMessage}
  //     />
  //   ));
  // };

  render() {
    const {
      authenticating,
      unauthorised,
      loginType,
      userClearAuthRequest,
      error,
      objNotification,
    } = this.props;
    return (
      <View style={styles.container}>
        <View styles={styles.video}>
          <ImageBackground
            source={ImagesIntro.startPoster}
            style={styles.fullScreen}
          >
            {this.renderVideo()}
          </ImageBackground>
        </View>
        <SafeAreaView style={styles.overlay}>
          <View style={styles.content}>
            <View style={styles.center}>
              <Image source={ImagesLogos.logo} style={styles.image} />
            </View>
            <View style={styles.buttonsContainer}>
              <View style={styles.buttons}>
                <Button
                  medium
                  style={styles.actionButton}
                  onPress={this.googleLoginHandler}
                  colour="google"
                  icon={socialIcons.googleLetter}
                  disabled={authenticating}
                >
                  Sign in with Google
                </Button>
              </View>
              <View style={styles.buttons}>
                <Button
                  medium
                  style={styles.actionButton}
                  onPress={this.facebookLoginHandler}
                  colour="facebook"
                  icon={socialIcons.facebookRound}
                  // disabled={loadingGoogle}
                >
                  Sign in with Facebook
                </Button>
              </View>
              <View style={styles.buttons}>
                <Button
                  medium
                  style={styles.actionButton}
                  onPress={this.onLogin}
                  colour="primary"
                  disabled={authenticating}
                >
                  Sign in with email
                </Button>
              </View>
              <View style={styles.buttons}>
                <Button
                  medium
                  style={styles.actionButton}
                  onPress={this.onRegister}
                  colour="black"
                  disabled={authenticating}
                >
                  Register
                </Button>
              </View>
            </View>
          </View>
        </SafeAreaView>
        {error && objNotification && (
          <Notification
            onClose={userClearAuthRequest}
            active={!!objNotification}
            type={objNotification.enumNotificationVariant.stringVal}
            message={objNotification.strMessage}
          />
        )}
      </View>
    );
  }
}

export default compose(withNotification, withAuthentication)(StartPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonsContainer: {
    marginTop: 30,
  },
  buttons: {
    width: "100%",
    flexDirection: "row",
    marginBottom: 15,
  },
  actionButton: {
    marginHorizontal: 10,
    width: "40%",
  },
  blankButton: {
    fontSize: 14,
    fontFamily: "Medium",
    color: "#FFFFFF",
    textAlign: "center",
    marginTop: 20,
  },
  overlay: {
    // ...StyleSheet.absoluteFillObject,
    position: "absolute",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  video: {
    width: Layout.window.width,
    height: Layout.window.height,
  },
  content: {
    margin: 10,
    marginBottom: 20,
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  image: {
    width: 150,
    height: 150,
  },
  center: {
    justifyContent: "center",
    alignItems: "center",
  },
  fullScreen: {
    width: Layout.window.width,
    height: Layout.screen.height,
  },
});
