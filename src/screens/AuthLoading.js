import React from "react";
import { Image } from "react-native";
import Layout from "config/constants";
import { ImagesIntro } from "config/AppIntro";
import PropTypes from "prop-types";
import { connect } from "react-redux";
class AuthLoading extends React.PureComponent {
  constructor(props) {
    super(props);
    console.log("I am running");
    if (props.authenticated) {
      props.navigation.navigate("Main");
    } else {
      props.navigation.navigate("Auth");
    }
  }

  componentDidMount() {
    console.log("I am coming from Authloading.js");
  }

  render() {
    return (
      <Image
        source={ImagesIntro.startPoster}
        style={{ width: Layout.window.width, height: Layout.window.height }}
      />
    );
  }
}

AuthLoading.propTypes = {
  authenticated: PropTypes.bool,
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  authenticated: state.auth.authenticated,
});

export default connect(mapStateToProps, null)(AuthLoading);
