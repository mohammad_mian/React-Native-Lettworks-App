import React from "react";
import { connect } from "react-redux";
import { StyleSheet, View } from "react-native";
import { Button } from "atoms";
import Container from "./components/Container";
import Heading from "./components/Heading";
import TopBar from "./components/TopBar";

class PersonaliseScreen extends React.PureComponent {
  onNext = () => {
    this.props.navigation.navigate("Location");
  };

  render() {
    const { user } = this.props;

    return (
      <Container>
        <TopBar onSkip={this.onNext} />

        <Heading
          title={`${
            user && user.firstName ? `${user.firstName},${"\n"}` : ""
          }Personalise${"\n"}your app`}
          subtitle="Want to get exactly what you're after? Personalise your profile so we can bring you the best properties just for you."
        />

        <View>
          <Button
            style={styles.buttonContainer}
            medium
            buttonStyle={styles.actionButton}
            onPress={this.onNext}
            colour="black"
          >
            Get Started
          </Button>
        </View>
        <View />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

export default connect(mapStateToProps, null)(PersonaliseScreen);

const styles = StyleSheet.create({
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  actionButton: {
    width: "70%",
    height: 50,
  },
});
