import React from "react";
import { Platform } from "react-native";
import { Notifications } from "expo";
import { isPermissionDenied } from "config/functions";
import TabsNav from "config/navigation/MainTabNavigator";
import { expo } from "../../app.json";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { actionUpdateUser } from "redux/actions/actionUser";
class MainScreen extends React.PureComponent {
  static router = TabsNav.router; // Not sure if need this

  state = {
    dismissWelcomeMessage: false,
  };

  componentDidMount() {
    this.getPushToken();
    this.getAppVersion();
  }

  componentDidUpdate() {
    if (!this.props.authenticated) {
      this.props.navigation.navigate("Auth");
    }
  }

  // Get the token that uniquely identifies this device to send push notifications
  getPushToken = async () => {
    try {
      if (await isPermissionDenied("NOTIFICATIONS")) return; // if true we do have permission and can move on to get expo and device token

      const expoToken = await Notifications.getExpoPushTokenAsync();
      let deviceToken = __DEV__
        ? {}
        : await Notifications.getDevicePushTokenAsync({}).catch();
      if (!deviceToken) deviceToken = {};
      const { pushToken } = this.props;
      if (
        Object.values(pushToken).length &&
        expoToken === pushToken.expoToken &&
        deviceToken.data === pushToken.deviceToken
      ) {
        console.log("no need to update push token");
        return;
      }
      await this.props.actionUpdateUser({
        pushToken: {
          expoToken,
          platform: Platform.OS,
          deviceToken: deviceToken.data,
          type: deviceToken.type,
        },
      });
    } catch (ex) {
      console.log("unable to get push token", ex);
    }
  };

  // Update app version
  getAppVersion = async () => {
    const { appVersion } = this.props;
    if (!expo || !expo || !expo.version) return;
    if (appVersion === expo.version) return;

    try {
      await this.props.actionUpdateUser({
        appVersion: expo.version,
      });
    } catch (ex) {
      console.log("ERROR VERSION", ex);
    }
  };

  render() {
    return <TabsNav navigation={this.props.navigation} />;
  }
}

MainScreen.propTypes = {
  authenticated: PropTypes.bool,
  pushToken: PropTypes.object,
  appVersion: PropTypes.string,
};

const mapStateToProps = (state, ownProps) => ({
  authenticated: state.auth.authenticated,
  pushToken: state.user.pushToken || {},
  appVersion: state.user.appVersion || "",
});

export default connect(mapStateToProps, { actionUpdateUser })(MainScreen);
