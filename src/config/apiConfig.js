import Constants from "expo-constants";

const { manifest } = Constants;

export const apiUrl =
  typeof manifest.packagerOpts === "object" && manifest.packagerOpts.dev
    ? `http://${manifest.debuggerHost
        .split(":")
        .shift()
        .concat(":8000")}/api/v1/`
    : "api.example.com";
