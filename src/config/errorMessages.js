export const FBLoginErrorMessage =
  "Sorry, facebook server is currently down. Please sign in using google or email.";
export const googleLoginErrorMessage =
  "Sorry, google server is currently down. Please sign in using facebook or email.";
export const defaultErrorMessage =
  "Sorry, there was an error logging you in. please try again";
