module.exports = {
  extends: "airbnb",
  parser: "babel-eslint",
  env: {
    jest: true,
  },
  rules: {
    "prefer-destructuring": false,
    "no-console": "off",
    "no-use-before-define": "off",
    "react/jsx-filename-extension": "off",
    "react/prop-types": "off",
    "comma-dangle": "off",
  },
  globals: {
    fetch: false,
  },
  env: {
    browser: true,
    node: true,
    jasmine: true,
  },
  settings: {
    "import/resolver": {
      node: {
        paths: ["src"],
      },
    },
  },
};
